# asmx Multi-CPU Assembler

This is my fork of the asmx multi-CPU assembler by Bruce Tomlin. The original version that I forked from came from [asmx-2.05b.zip](http://xi6.com/files/asmx-2.0b5.zip) as found at [http://xi6.com/projects/asmx/](http://xi6.com/projects/asmx/), and downloaded on 2017-09-30.

The original version is marked "Copyright 1998-2007 Bruce Tomlin", and does not specify a distribution license, nor any distribution restrictions. I'll append my own fork version numbers to the end of the original distribution's version number.

My fork is provided as-is, without any warranty.

Enjoy!
Mark J. Blair &lt;nf6x@nf6x.net&gt;

## Changes

### 2.0b5\_NF6X\_20170930

* Compilation fix.

* Added fork version to version number string.


